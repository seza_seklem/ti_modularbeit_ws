function hex2bin(hex) {
  //hex in binär
  var hexAsInt = parseInt(hex, 16);
  return (hexAsInt >>> 0).toString(2);
}

function cpToUTF8Binary(...codepoints) {
    var result = "";
for (i = 0; i < codepoints.length; ++i) {
   result += " 0" + hex2bin(codepoints[i].split("+")[1]);
  }
  
    return result;
}


function showResults() {
  var clientInput = document.getElementById("textfield1").value;
  var splitted = clientInput.split(" ");
  var binString = "";

  //hier in hex
  for(index = 0; index < splitted.length;index++){
    binString += cpToUTF8Binary(splitted[index]);
  }

  //suche nach parent
  var parent = document.getElementById("convertedBinary");
  //erstelle hier future child
  var result = document.createTextNode(binString);
  //dann gucken ob es schon ein child gibt
  if (parent.hasChildNodes()) {
    //lösche alle children wenn parent schon ein child hat
    while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
    }
    //dann füge new child ein
    parent.appendChild(result);
  } else {
    //füge new child ein
    parent.appendChild(result);
  }
}
