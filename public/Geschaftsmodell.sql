
/*
Beschreibung, wie ich diese Datenbank erstellt habe
  1. Tabellen mit allen Entitäten und Beziehungen erstellt
  2. suchte nach Entitäten, die miteinander verschmolzen werden konnten (1-1 Beziehungen)
  3. Beziehungstabellen verwenden die Primärschlüssel der einzelnen Entitäten als            Attribute
  4. Beziehungstabellen verwenden das Attribut der Entität "auf der n-Seite" als              Primärschlüssel
  5. die Beziehungstabellen mit den entsprechenden Entity-Tabellen verschmolzen, wenn        möglich
  6. alle Tabellen erstellt, die übrig sind
*/
create Database Geschäftsmodell;
	use Geschäftsmodell;
    
    create table Maschinenbauer(
		Name varchar(30),
        Nachname varchar(30),
        EMailAdresse text,
        Telefonnummer int,
        Primary Key(Name,Nachname)
    );
    insert into Maschinenbauer(Name, Nachname, EMailAdresse, Telefonnummer)
		value
        ("MBauerName1","MBauerNachname1","email@example1.com", 1234567),
        ("MBauerName2","MBauerNachname1","email@example2.com", 7654321);
    
    create table Kunde(
		Kundennummer int primary key,
		Name varchar(30),
        Nachname varchar(30),
        EMailAdresse text,
        Telefonnummer int,
        MName varchar(30) references Maschinenbauer(Name),
        MNachname varchar(30) references Maschinenbauer(Nachname)
    );
    insert into Kunde(Kundennummer, Name, Nachname, EMailAdresse, Telefonnummer,MName,MNachname)
		value
        (1,"Kunde1","Kunde11","kundenmail1@mail.com",11111, (SELECT Name from Maschinenbauer where Name="MBauerName1"), (SELECT Nachname from Maschinenbauer where Name="MBauerName1")),
        (2,"Kunde2","Kunde22","kundenmail2@mail.com",22222, (SELECT Name from Maschinenbauer where Name="MBauerName2"), (SELECT Nachname from Maschinenbauer where Name="MBauerName2"));
    
    
    create table Maschine(
		MaschinenID int primary key,
        DatumDerLetztenWartung varchar(10),
        Kundennummer int references Kunde(Kundennummer)
    );
    insert into Maschine(MaschinenID, DatumDerLetztenWartung,Kundennummer)
		value
        (1,"01.01.2001", (Select Kundennummer from Kunde where Kundennummer=1)),
		(2,"01.01.2001", (Select Kundennummer from Kunde where Kundennummer=2));

    
    create Table Ersatzteile(
		Ersatzteilnummer int primary key,
        Bezeichnung text,
        MaschinenID int references Maschine(MaschinenID)
    );
    insert into Ersatzteile(Ersatzteilnummer, Bezeichnung, MaschinenID)
		value
        (111,"Ersatzteil1", (SELECT MaschinenID from Maschine WHERE MaschinenID=1)),
		(222,"Ersatzteil2", (SELECT MaschinenID from Maschine WHERE MaschinenID=1)),
		(333,"Ersatzteil3", (SELECT MaschinenID from Maschine WHERE MaschinenID=2));
        
    
    
    create Table Servicetechniker(
		Mitarbeiternummer int primary key,
        Name varchar(30),
        Nachname varchar(30),
        Arbeitsvertragbeginn varchar(10),
        Arbeitsvertragende varchar(10)
    );
	insert into Servicetechniker(Mitarbeiternummer, Name,Nachname,Arbeitsvertragbeginn,Arbeitsvertragende)
		value
        (1,"Techniker1","Techniker11","01.01.2001", "01.01.2011"),
        (2,"Techniker2","Techniker22","02.02.2002", "02.02.2022");
    
    /* Tabelle kann nicht weiter noramlisiert werden */
    create table betreut(
		MaschinenID int references Maschine(MaschinenID),
        Mitarbeiternummer int references Servicetechniker(Mitarbeiternummer)
    );
    insert into betreut(MaschinenID, Mitarbeiternummer)
		value
        ((Select MaschinenID from Maschine where MaschinenID=1),(Select Mitarbeiternummer from Servicetechniker where Mitarbeiternummer=1)),
		((Select MaschinenID from Maschine where MaschinenID=2),(Select Mitarbeiternummer from Servicetechniker where Mitarbeiternummer=2));
        
    
     /* Tabelle kann nicht weiter noramlisiert werden */
       create table wirdInformiert(
		MaschinenID int references Maschine(MaschinenID),
        Mitarbeiternummer int references Servicetechniker(Mitarbeiternummer)
    );
       insert into wirdInformiert(MaschinenID, Mitarbeiternummer)
		value
        ((Select MaschinenID from Maschine where MaschinenID=1),(Select Mitarbeiternummer from Servicetechniker where Mitarbeiternummer=1)),
		((Select MaschinenID from Maschine where MaschinenID=2),(Select Mitarbeiternummer from Servicetechniker where Mitarbeiternummer=2));
    
    /** Kunde kann Attribute der Maschine vor der Initialisierung nicht referenzieren, daher musste ich die Tabelle ändern **/
    alter table Kunde
		add column MaschinenID int references Maschine(MaschinenID) after MNachname;
        
	update Kunde set MaschinenID=(Select MaschinenID from Maschine where MaschinenID=1) where Kundennummer=1;
    update Kunde set MaschinenID=(Select MaschinenID from Maschine where MaschinenID=2) where Kundennummer=2;
        
        
        
	
    